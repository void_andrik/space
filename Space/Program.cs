﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Space
{
    static class Program
    {
        public static string fileName;
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length == 1)
                fileName = args[0];
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }
    }
}
