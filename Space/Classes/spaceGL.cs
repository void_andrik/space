﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Space
{
    class spaceGL: IDisposable
    {
        private bool _ctrlLoaded = false;
        public bool ctrlLoaded
        {
            get { return this._ctrlLoaded; }
        }
        private GLControl _ctrl;    //Контрол, в который будет идти отрисовка

        private double _width;      //Размеры сцены
        private double _height;     //Размеры сцены

        public double _scale = 1;   //Масштаб
        double minimalRadius = 2;

        //Текстуры
        public Dictionary<string, int> textures = new Dictionary<string, int>();

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="glControl">GLControl для отрисовки</param>
        /// <param name="width">Ширина области рисования</param>
        /// <param name="height">Высота области рисования</param>
        public spaceGL(GLControl glControl, double width, double height)
        {
            this._ctrl = glControl;
            this._height = height;
            this._width = width;
        }

        public void setLoaded()
        {
            loadTextures();
            _ctrlLoaded = true;
        }

        /// <summary>
        /// Инициализация
        /// </summary>
        public void reset()
        {
            if (!_ctrlLoaded) return;
            GL.ClearColor(Color.Black);
            double scX = (double)_ctrl.Width / (double)this._width;
            double scY = (double)_ctrl.Height / (double)this._height;
            if (scX < scY)
                this._scale = scX;
            else
                this._scale = scY;
            this.resize();
        }

        /// <summary>
        /// Обрабатывыаем изменение размера области отрисовки
        /// </summary>
        public void resize()
        {
            if (!_ctrlLoaded) return;
            double trX = _ctrl.Width / 2;
            double trY = _ctrl.Height / 2;
            GL.MatrixMode(MatrixMode.Projection);
            GL.PushMatrix();
            GL.LoadIdentity();
            GL.Ortho(0, _ctrl.Width, 0, _ctrl.Height, -1, 1);
            GL.Viewport(0, 0, _ctrl.Width, _ctrl.Height);

            GL.MatrixMode(MatrixMode.Modelview);
            GL.PushMatrix();
            GL.LoadIdentity();

            GL.Translate(trX, trY, 0);
            GL.Scale(this._scale, this._scale, 0);
            _ctrl.Invalidate();
        }

        #region Privates

        /// <summary>
        /// Начало отрисовки сцены
        /// </summary>
        private void paintStart()
        {
            if (!_ctrlLoaded) return;
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        }

        /// <summary>
        /// Окончание отрисовки сцены
        /// </summary>
        private void paintEnd()
        {
            if (!_ctrlLoaded) return;
            GL.Flush();
            this._ctrl.SwapBuffers();
        }

        /// <summary>
        /// Загрузка имеющихся текстур
        /// </summary>
        private void loadTextures()
        {
            if (textures.Count == 0)
            {
                string[] texFiles = Directory.GetFiles("textures", "*.bmp");
                foreach (string texFile in texFiles)
                {
                    string texName = Path.GetFileNameWithoutExtension(texFile);
                    int texID = loadTexture(texFile);
                    textures.Add(texName, texID);
                }
            }
        }

        /// <summary>
        /// Загрузка текстуры из файла
        /// </summary>
        /// <param name="filename">Имя файла</param>
        /// <returns></returns>
        private int loadTexture(string filename)
        {
            Bitmap bitmap = new Bitmap(filename);

            int tex;
            GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);

            GL.GenTextures(1, out tex);
            GL.BindTexture(TextureTarget.Texture2D, tex);

            BitmapData data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width, data.Height, 0,
                OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
            bitmap.UnlockBits(data);


            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);

            return tex;
        }

        /// <summary>
        /// Нарисовать круг
        /// </summary>
        /// <param name="x">X</param>
        /// <param name="y">Y</param>
        /// <param name="r">Радиус</param>
        /// <param name="color">Цвет</param>
        private void drawCircle(double x, double y, double r, Color color)
        {
            if (!_ctrlLoaded) return;
            int sides = 45;
            double realRadius = r * this._scale;
            if (realRadius < minimalRadius)
                r = minimalRadius / this._scale;
            GL.Begin(BeginMode.Polygon);
            GL.Color3(color);
            for (int a = 0; a <= 360; a += 360 / sides)
            {
                double heading = a * 3.1415926535897932384626433832795 / 180;
                GL.Vertex3(x + Math.Cos(heading) * r, y + Math.Sin(heading) * r, 0);
            }
            GL.End();
        }

        private void drawTexture(double x, double y, int texture, double size = 40, bool isMask = false)
        {
            if (!_ctrlLoaded) return;
            if (isMask)
            {
                GL.Enable(EnableCap.Blend);
                GL.BlendFunc(BlendingFactorSrc.Zero, BlendingFactorDest.One);
            }
            else
                GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One);
            x -= size / 2;
            y -= size / 2;
            GL.BindTexture(TextureTarget.Texture2D, texture);
            GL.Enable(EnableCap.Texture2D);
            GL.Begin(BeginMode.Quads);
            GL.Color3(Color.White);

            GL.TexCoord2(0, 0);
            GL.Vertex3(x, y, 0);

            GL.TexCoord2(1, 0);
            GL.Vertex3(x + size, y, 0);

            GL.TexCoord2(1, 1);
            GL.Vertex3(x + size, y + size, 0);

            GL.TexCoord2(0, 1);
            GL.Vertex3(x, y + size, 0);

            GL.End();
            GL.Disable(EnableCap.Texture2D);
        }
        #endregion

        public void scaleUp()
        {
            _scale += _scale * 0.1;
            this.resize();
        }

        public void scaleDown()
        {
            _scale -= _scale * 0.1;
            if (_scale < 0)
                _scale = 0;
            this.resize();
        }

        public void drawScene(spaceSystem space)
        {
            if (!_ctrlLoaded) return;
            paintStart();
            if (space != null)
            {
                foreach (spaceObject obj in space.spaceObjects)
                {
                    double x = space.getX(obj);
                    double y = space.getY(obj);
                    double realRadius = obj.radius * this._scale;
                    if (string.IsNullOrEmpty(obj.textureName) || realRadius<minimalRadius)
                        drawCircle(x, y, obj.radius, obj.color);
                    else
                    {
                        drawTexture(x, y, textures[obj.textureName], obj.radius * 2, true);
                        drawTexture(x, y, textures[obj.textureName], obj.radius * 2, false);
                    }

                    foreach (DoublePoint p in obj.tail)
                        drawCircle(space.getX(p.X), space.getY(p.Y), obj.radius/3, obj.color);
                }
            }
            paintEnd();
        }

        public void Dispose()
        {
            foreach (KeyValuePair<string, int> tex in this.textures)
            {
                GL.DeleteTexture(tex.Value);
            }
        }
    }
}
