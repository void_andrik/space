﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Drawing;

namespace Space
{

    /// <summary>
    /// Космический объект
    /// </summary>
    public class spaceObject
    {

        #region Properties

        public string name = "";

        public double X = 0;        //Начальные координаты
        public double Y = 0;        //Начальные координаты

        public double radius = 5;   //Радиус
        public double mass = 0;     //Масса

        public double vX = 0;       //Начальные скорости
        public double vY = 0;       //Начальные скорости

        //Цвет
        [XmlElement("color")]
        public string hexColor = "#FFFFFF";
        [XmlIgnore]
        public Color color { get { return ColorTranslator.FromHtml(this.hexColor); } }

        public int isFixed = 0;             //Фиксировано ли положение
        public int tailLength = 0;                //Длина хвоста
        public string textureName = "";     //Название текстуры

        [XmlIgnore]
        public List<mass_X_Y> others = new List<mass_X_Y>();
        [XmlIgnore]
        public List<DoublePoint> tail = new List<DoublePoint>();
        [XmlIgnore]
        private int updCnt = 0;

        #endregion

        #region Methods

        /// <summary>
        /// Расчёт ускорения к объекту
        /// </summary>
        /// <param name="obj">Объект spaceObject</param>
        public void calcAccelTo(spaceObject obj)
        {
            others.Add(new mass_X_Y(obj));
        }

        private double fX(double localX)
        {
            double result = 0;
            foreach (mass_X_Y p in this.others)
            {
                double r = Math.Sqrt(Math.Pow(p.X - localX, 2) + Math.Pow(p.Y - this.Y, 2));
                result += p.mass * (p.X - localX) / Math.Pow(r, 3);
            }
            return result;
        }

        private double fY(double localY)
        {
            double result = 0;
            foreach (mass_X_Y p in this.others)
            {
                double r = Math.Sqrt(Math.Pow(p.X - this.X, 2) + Math.Pow(p.Y - localY, 2));
                result += p.mass * (p.Y - localY) / Math.Pow(r, 3);
            }
            return result;
        }


        /// <summary>
        /// Пересчитать X и vX
        /// </summary>
        /// <param name="T"></param>
        private void calcX(double T = 1)
        {
            double k1 = T * this.fX(this.X);
            double q1 = T * this.vX;

            double k2 = T * this.fX(this.X + q1 / 2.0);
            double q2 = T * (this.vX + k1 / 2.0);

            double k3 = T * this.fX(this.X + q2 / 2.0);
            double q3 = T * (this.vX + k2 / 2.0);

            double k4 = T * this.fX(this.X + q3);
            double q4 = T * (this.vX + k3);

            this.vX += (k1 + 2 * k2 + 2 * k3 + k4) / 6.0;
            this.X += (q1 + 2 * q2 + 2 * q3 + q4) / 6.0;
        }

        /// <summary>
        /// Пересчитать Y и vY
        /// </summary>
        /// <param name="T"></param>
        private void calcY(double T = 1)
        {
            double k1 = T * this.fY(this.Y);
            double q1 = T * this.vY;

            double k2 = T * this.fY(this.Y + q1 / 2.0);
            double q2 = T * (this.vY + k1 / 2.0);

            double k3 = T * this.fY(this.Y + q2 / 2.0);
            double q3 = T * (this.vY + k2 / 2.0);

            double k4 = T * this.fY(this.Y + q3);
            double q4 = T * (this.vY + k3);

            this.vY += (k1 + 2 * k2 + 2 * k3 + k4) / 6.0;
            this.Y += (q1 + 2 * q2 + 2 * q3 + q4) / 6.0;
        }

        /// <summary>
        /// Обновить положение объекта
        /// </summary>
        /// <param name="p"></param>
        public void update(spaceSystem p)
        {
            //Если объект не фиксирован
            if (this.isFixed == 0)
            {
                //Отрисовываем хвост каждые 20 итераций обновления
                updCnt += 1;
                if (updCnt > 20 * p.multiplier)
                {
                    updCnt = 0;
                    DoublePoint tailEnd = new DoublePoint(this.X, this.Y);
                    tail.Add(tailEnd);
                    if (tail.Count > tailLength)
                        tail.RemoveAt(0);
                }
                this.calcX(p.T);
                this.calcY(p.T);
            }
            //Очистка списка расчёта ускорения
            this.others.Clear();
        }
        #endregion

        public override string ToString()
        {
            return this.name;
        }
    }

}
