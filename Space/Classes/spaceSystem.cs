﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;

namespace Space
{

    /// <summary>
    /// Система космических объектов
    /// </summary>
    public class spaceSystem: ICloneable
    {
        #region Properties

        public double T = 0.00001;  //Точность рассчётов(шаг)
        public int multiplier = 10;
        public double Width = 1;    //Ширина поля обзора
        public double Height = 1;   //Высота поля обзора

        [XmlIgnore]
        public spaceObject center
        {
            get
            {
                if (!string.IsNullOrEmpty(this._center))
                {
                    spaceObject result = this.spaceObjects.Find(x => x.name == this._center);
                    return result;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (value != null)
                    this._center = value.name;
                else
                    this._center = "";
            }
        }
        [XmlElement("center")]
        public string _center = "";

        public List<spaceObject> spaceObjects = new List<spaceObject>();

        #endregion

        #region Methods
        
        /// <summary>
        /// Загрузить из файла
        /// </summary>
        /// <param name="fileName">Имя йафла</param>
        /// <returns>Если загружено успешно, то True</returns>
        public static spaceSystem load(string fileName)
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(spaceSystem));
            TextReader reader = new StreamReader(fileName);
            object obj = deserializer.Deserialize(reader);
            spaceSystem XmlData = (spaceSystem)obj;
            reader.Close();
            return XmlData;
        }

        /// <summary>
        /// Сохранить в файл
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        /// <returns>Если сохранено успешно, то True</returns>
        public bool save(string fileName)
        {
            try
            {
                XmlSerializer xml = new XmlSerializer(this.GetType());
                FileStream file = File.Open(fileName, FileMode.OpenOrCreate);
                xml.Serialize(file, this);
                file.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }
        
        /// <summary>
        /// Обновить состояние системы
        /// </summary>
        /// <returns></returns>
        public bool update()
        {
            try
            {
                for (int cnt = 0; cnt < multiplier; cnt++)
                {
                    //Расчёт ускорений
                    for (int i = 0; i < this.spaceObjects.Count; i++)
                    {
                        for (int j = 0; j < this.spaceObjects.Count; j++)
                            if (i != j)
                                this.spaceObjects[i].calcAccelTo(this.spaceObjects[j]);
                    }
                    //Обновление координат
                    for (int i = 0; i < this.spaceObjects.Count; i++)
                        this.spaceObjects[i].update(this);
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public double getX(spaceObject obj)
        {
            if (center != null)
                return obj.X - center.X;
            else
                return obj.X;
        }

        public double getX(double X)
        {
            if (center != null)
                return X - center.X;
            else
                return X;
        }

        public double getY(spaceObject obj)
        {
            if (center != null)
                return obj.Y - center.Y;
            else
                return obj.Y;
        }

        public double getY(double Y)
        {
            if (center != null)
                return Y - center.Y;
            else
                return Y;
        }

        public void speedUp()
        {
            this.multiplier += (int)Math.Ceiling(this.multiplier * 0.1);
            if (this.multiplier >= 1000 && this.T < 0.0001/*9E5*/)
            {
                this.T = this.T * 10;
                this.multiplier = 100;
            }
            if (this.multiplier >= 1000)
                this.multiplier = 1000;
        }

        public void speedDown()
        {
            this.multiplier -= (int)Math.Ceiling(this.multiplier * 0.1);
            if (this.multiplier == 1)
            {
                this.T = this.T / 100;
                this.multiplier = 100;
            }
        }

        #endregion

        public object Clone()
        {
            //return this.MemberwiseClone();
            spaceSystem clone = new spaceSystem();
            clone.T = this.T;
            clone.multiplier = this.multiplier;
            clone.Width = this.Width;
            clone.Height = this.Height;
            clone._center = this._center;
            clone.spaceObjects = new List<spaceObject>();
            foreach (spaceObject obj in this.spaceObjects)
            {
                spaceObject cloneObject = new spaceObject();
                cloneObject.name = obj.name;
                cloneObject.X = obj.X;
                cloneObject.Y = obj.Y;
                cloneObject.radius = obj.radius;
                cloneObject.mass = obj.mass;
                cloneObject.vX = obj.vX;
                cloneObject.vY = obj.vY;
                cloneObject.hexColor = obj.hexColor;
                cloneObject.isFixed = obj.isFixed;
                cloneObject.tailLength = obj.tailLength;
                cloneObject.textureName = obj.textureName;
                clone.spaceObjects.Add(cloneObject);
            }

            return clone;
        }

    }
}
