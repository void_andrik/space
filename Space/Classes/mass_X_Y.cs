﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space
{
    public class mass_X_Y
    {
        public double mass;
        public double X;
        public double Y;

        public mass_X_Y(spaceObject obj)
        {
            this.mass = obj.mass;
            this.X = obj.X;
            this.Y = obj.Y;
        }
    }
    
    public class DoublePoint
    {
        public double X;
        public double Y;
        public DoublePoint(double X, double Y)
        {
            this.X = X;
            this.Y = Y;
        }
    }
}
