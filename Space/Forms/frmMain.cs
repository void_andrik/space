﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Space
{
    public partial class frmMain : Form
    {
        private spaceGL _gl;                            //Работа с графикой
        private spaceSystem solar = new spaceSystem();  //Солнечная система
        private string currentFileName;                 //Текущий открытый файл
        private bool _modelRun = false;                 //Запущено ли моделирование
        const string appTitle = "Моделирование движения тел под силой притяжения";

        /// <summary>
        /// Инициализация формы
        /// </summary>
        public frmMain()
        {
            this.MouseWheel += new MouseEventHandler(frmMain_MouseWheel);
            InitializeComponent();
            this.Text = appTitle;
            //Если при запуске в параметре указан файл - открываем его
            if (!string.IsNullOrEmpty(Program.fileName))
                loadFile(Program.fileName);
            else
            //Иначе создаём пустую сцену
                _gl = new spaceGL(glCtrl, 100, 100);
        }

        /// <summary>
        /// Загрузка GL-контрола
        /// </summary>
        private void glCtrl_Load(object sender, EventArgs e)
        {
            //После того как GL-контрол загружен в нём можно рисовать
            _gl.setLoaded();
            _gl.reset();
        }

        /// <summary>
        /// Отрисовка сцены
        /// </summary>
        private void glCtrl_Paint(object sender, PaintEventArgs e)
        {
            _gl.drawScene(solar);
        }
        
        /// <summary>
        /// Изменение размера сцены
        /// </summary>
        private void glCtrl_Resize(object sender, EventArgs e)
        {
            _gl.resize();
        }

        /// <summary>
        /// Загрузка файла в модель
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        private void loadFile(string fileName)
        {
            //Если имя задано - загружаем модель из файла
            if (!string.IsNullOrEmpty(fileName))
            {
                solar = spaceSystem.load(fileName);
                currentFileName = fileName;
            }
            else
            //Иначе модель будет пустая
                solar = null;
            bool glLoaded=false;
            if (_gl != null)
            {
                glLoaded = _gl.ctrlLoaded;
                _gl.Dispose();
            }
            _gl = new spaceGL(glCtrl, solar.Width, solar.Height);
            if (glLoaded)
                _gl.setLoaded();
            _gl.reset();
            updateProperties(solar);
        }

        /// <summary>
        /// Обновление модели
        /// </summary>
        private void updateTimer_Tick(object sender, EventArgs e)
        {
            if (_modelRun)
            {
                solar.update();
                updateValues(solar);
            }
            glCtrl.Invalidate();
        }

        #region "Меню"
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openXML.ShowDialog() == DialogResult.OK)
            {
                loadFile(openXML.FileName);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void runToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pauseModel(false);
        }

        private void pauseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pauseModel(true);
        }

        private void reloadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(currentFileName))
                loadFile(currentFileName);
            glCtrl.Invalidate();
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool currentModelState = _modelRun;
            _modelRun = false;
            frmProperties p = new frmProperties(solar);
            if (p.ShowDialog() == DialogResult.OK)
            {
                solar = (spaceSystem)p._space.Clone();
                bool glLoaded = false;
                if (_gl != null)
                    glLoaded = _gl.ctrlLoaded;
                _gl = new spaceGL(glCtrl, solar.Width, solar.Height);
                if (glLoaded)
                    _gl.setLoaded();
                _gl.reset();
                updateProperties(solar);
            }
            _modelRun = currentModelState;
        }

        #endregion

        /// <summary>
        /// Обработка нажатий клавиш
        /// </summary>
        private void glCtrl_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                // *(плюс) - увеличить скорость
                case Keys.Add: solar.speedUp();
                    break;
                // /(минус) - уменьшить скорость
                case Keys.Subtract: solar.speedDown();
                    break;
                // >(больше) - следующий центр
                case (Keys)190: 
                    int i1 = solar.spaceObjects.FindIndex(x => x.name == solar._center);
                    if (i1 >= solar.spaceObjects.Count-1)
                        i1 = 0;
                    else
                        i1++;
                    solar.center = solar.spaceObjects.ElementAt<spaceObject>(i1);
                    cbCenter.Text = solar._center;
                    break;
                // <(меньше) - предыдущий центр
                case (Keys)188:
                    int i2 = solar.spaceObjects.FindIndex(x => x.name == solar._center);
                    if (i2 <= 0)
                        i2 = solar.spaceObjects.Count - 1;
                    else
                        i2--;
                    solar.center = solar.spaceObjects.ElementAt<spaceObject>(i2);
                    cbCenter.Text = solar._center;
                    break;
            }
        }

        /// <summary>
        /// Обработка прокрутки колесом мыши
        /// </summary>
        public void frmMain_MouseWheel(object sender, EventArgs e)
        {
            if ((e as HandledMouseEventArgs).Delta > 0)
                _gl.scaleUp();
            else if ((e as HandledMouseEventArgs).Delta < 0)
                _gl.scaleDown();
        }

        private void frmMain_KeyDown(object sender, KeyEventArgs e)
        {
            glCtrl_KeyDown(sender, e);
        }

        private void updateProperties(spaceSystem space)
        {
            tbW.Text = space.Width.ToString();
            tbH.Text = space.Height.ToString();
            cbCenter.Items.Clear();
            foreach (spaceObject o in space.spaceObjects)
            {
                cbCenter.Items.Add(o.name);
            }
            cbCenter.Text = space._center;
            updateValues(space);
        }

        private void updateValues(spaceSystem space)
        {
            if (space != null)
            {
                spaceObject o = solar.center;
                tbX.Text = o.X.ToString();
                tbY.Text = o.Y.ToString();
                tbVX.Text = o.vX.ToString();
                tbVY.Text = o.vY.ToString();
                tbMass.Text = o.mass.ToString();
                tbRadius.Text = o.radius.ToString();
            }
        }

        private void cbCenter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (solar != null)
                solar._center = cbCenter.Text;
        }

        private void pauseModel(bool act)
        {
            if (act) //Пауза
            {
                _modelRun = false;
                this.Text = appTitle;

            }
            else //Работаем
            {
                _modelRun = true;
                this.Text = appTitle + " (Запущено)";

            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(currentFileName))
                solar.save(currentFileName);
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveXML.FileName = currentFileName;
            if (saveXML.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                solar.save(saveXML.FileName);
        }
    }
}
