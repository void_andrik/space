﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Space
{
    public partial class frmProperties : Form
    {
        public spaceSystem _space;

        public frmProperties(spaceSystem space)
        {
            InitializeComponent();
            _space = (spaceSystem)space.Clone();
            tbW.Text = _space.Width.ToString();
            tbH.Text = _space.Height.ToString();
            buildList();
        }

        private void buildList()
        {
            lbObjects.Items.Clear();
            cbCenter.Items.Clear();
            foreach (spaceObject o in _space.spaceObjects)
            {
                cbCenter.Items.Add(o.name);
                lbObjects.Items.Add(o);
            }
            cbCenter.Text = _space._center;
            if (lbObjects.Items.Count > 0)
                lbObjects.SelectedIndex = 0;

        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            _space.Width = double.Parse(tbW.Text);
            _space.Height = double.Parse(tbH.Text);
            _space._center = cbCenter.Text;
        }

        private void lbObjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            spaceObject o = (spaceObject)lbObjects.SelectedItem;
            tbName.Text = o.name;
            tbX.Text = o.X.ToString();
            tbY.Text = o.Y.ToString();
            tbVX.Text = o.vX.ToString();
            tbVY.Text = o.vY.ToString();
            tbMass.Text = o.mass.ToString();
            tbRadius.Text = o.radius.ToString();
            tbColor.Text = o.hexColor;
            btnColor.BackColor = ColorTranslator.FromHtml(tbColor.Text);
        }

        private void btnApplyObject_Click(object sender, EventArgs e)
        {
            spaceObject o = (spaceObject)lbObjects.SelectedItem;
            int nameCnt = 0;
            foreach (spaceObject obj in _space.spaceObjects)
                if (obj != o && obj.name == tbName.Text)
                    nameCnt++;
            if (nameCnt == 0)
            {
                o.name = tbName.Text;
                o.X = float.Parse(tbX.Text);
                o.Y = float.Parse(tbY.Text);
                o.vX = float.Parse(tbVX.Text);
                o.vY = float.Parse(tbVY.Text);
                o.mass = float.Parse(tbMass.Text);
                o.radius = float.Parse(tbRadius.Text);
                o.hexColor = tbColor.Text;
            }
            else
                MessageBox.Show("Объект с таким именем уже существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void frmProperties_Load(object sender, EventArgs e)
        {

        }

        private void btnColor_Click(object sender, EventArgs e)
        {
            colorDialog1.Color = ColorTranslator.FromHtml(tbColor.Text);
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tbColor.Text = ColorTranslator.ToHtml(colorDialog1.Color);
                btnColor.BackColor = ColorTranslator.FromHtml(tbColor.Text);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string objName = string.Format("Объект {0}", lbObjects.Items.Count);
            spaceObject obj = new spaceObject();
            obj.name = objName;
            _space.spaceObjects.Add(obj);
            buildList();
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (lbObjects.SelectedItems.Count > 0)
            {
                spaceObject o = (spaceObject)lbObjects.SelectedItem;
                string delName = o.name;
                for (int i = 0; i < _space.spaceObjects.Count; i++)
                    if (_space.spaceObjects[i].name == delName)
                        _space.spaceObjects.RemoveAt(i);
                buildList();
            }
        }
    }
}
